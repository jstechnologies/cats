<?php
include_once('./config.php');

# Connect to the database
mysql_pconnect(DATABASE_HOST, DATABASE_USER, DATABASE_PASS) or die("Could not connect");
mysql_select_db(DATABASE_NAME) or die("Could not select database");

 $sql = sprintf("SELECT skill as id, skill as name from skills WHERE skill LIKE '%%%s%%' ORDER BY skill LIMIT 10", mysql_real_escape_string($_GET["q"]));
 $result = mysql_query($sql);
 $arr = array();
 while($obj = mysql_fetch_object($result)) {
    $arr[] = $obj;
}

# JSON-encode the response
$json_response = json_encode($arr);


# Return the response
echo $json_response;

?>