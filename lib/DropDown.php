<?php
/**
 * CATS
 * Attachments Library
 *
 * Copyright (C) 2005 - 2007 Cognizo Technologies, Inc.
 *
 *
 * The contents of this file are subject to the CATS Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.catsone.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "CATS Standard Edition".
 *
 * The Initial Developer of the Original Code is Cognizo Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Cognizo Technologies, Inc. All Rights Reserved.
 *
 *
 * @package    CATS
 * @subpackage Library
 * @copyright Copyright (C) 2005 - 2007 Cognizo Technologies, Inc.
 * @version    $Id: Attachments.php 3793 2007-12-03 22:58:01Z andrew $
 */


/**
 *	DropDowns		Library
 *	@package    	CATS
 *	@subpackage 	Library
 */
class DropDown
{
    private $_db;


    public function __construct()
	
    {
        $this->_db = DatabaseConnection::getInstance();
    }

	
    /**
     * Get CountryDropDown values
     *
     * @param integer Attachment ID.
     * @param boolean Attempt to remove the actual file from the filesystem?
     *                Otherwise, only the database record will be removed.
     * @return boolean True if successful; false otherwise.
     */
    public function getCountries()
	{
		$sql = sprintf(
            "SELECT
                value AS countryname
            FROM
                dropdown"
        );

        return $this->_db->getAllAssoc($sql);
	}
    

}

?>
